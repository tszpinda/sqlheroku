package sqlheroku

import (
	"fmt"
	"os"
	"strings"
)

type conn struct {
	username, password, host, database, args string
}

func (c *conn) String() string {
	//"tcp:localhost:3306*testDb/user/passwd"
	return fmt.Sprintf("tcp:%s:3306*%s/%s/%s", c.host, c.database, c.username, c.password)
}

func GetUrl() string {
	url := os.Getenv("CLEARDB_DATABASE_URL")
	if url == "" {
		return ""
	}
	fmt.Println("Heroku db url:", url)
	return parse(url).build()
}

func parse(url string) *conn {
	c := &conn{}
	s := strings.Split(url, "/")

	dbArgs := strings.Split(s[3], "?")

	c.database = dbArgs[0]
	c.args = dbArgs[1]

	userPassHost := strings.Split(s[2], "@")

	c.host = userPassHost[1]

	userPass := strings.Split(userPassHost[0], ":")
	c.username = userPass[0]
	c.password = userPass[1]

	return c
}

func (c *conn) build() string {
	return c.String()
}
