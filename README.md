Builds connection string from heroku enviroment variable to be ready to use with go language.

Usage:

	import ("bitbucket.org/tszpinda/sqlheroku")

	goDbUrl := sqlheroku.GetUrl()
	db, err := sql.Open("mymysql", goDbUrl)

