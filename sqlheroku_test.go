package sqlheroku

import (
	"testing"
)

func TestParse(t *testing.T) {
	var url = "mysql://kads93kmjjd:dasda34fdfd@us-cdbr-east.cleardb.com/heroku_a42bd8b0e8aa600?reconnect=true"
	c := parse(url)
	if c.username != "kads93kmjjd" {
		t.Error("invalid user", c.username)
	}
	if c.password != "dasda34fdfd" {
		t.Error("invalid password", c.password)
	}
	if c.host != "us-cdbr-east.cleardb.com" {
		t.Error("invalid host", c.host)
	}
	if c.database != "heroku_a42bd8b0e8aa600" {
		t.Error("invalid database", c.database)
	}
	//if c.args != "reconnect=true" {
	//	t.Error("invalid args", c.args)
	//}
}

func TestBuild(t *testing.T) {
	c := conn{"user", "passwd", "localhost", "testDb", ""}
	url := c.build()
	if url != "tcp:localhost:3306*testDb/user/passwd" {
		t.Error("invalid url", url)
	}
}
